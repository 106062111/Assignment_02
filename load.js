var loadState = {
    preload: function() {
        game.load.image('menubg', 'assets/menu-bg.jpg');
        // Loat game sprites.
        game.load.image('background', 'assets/Nebula Aqua-Pink.png');
        //game.load.image('background_cover', 'assets/Stars Small_1.png');
        game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemy_bullet', 'assets/bullet1.png');
        game.load.spritesheet('enemy', 'assets/enemy-big.png', 32, 32);


        /// Load block spritesheet.
        game.load.spritesheet('block1', 'assets/block1.png', 28, 28);
        game.load.spritesheet('block2', 'assets/block2.png', 28, 28);
        

        /// ToDo 1: Load spritesheet
        ///      The name of sprite is 'player'.
        ///      The spritesheet filename is assets/MARIO.png
        ///      A frame size is 32 x 54.
        game.load.spritesheet('player', 'assets/Spaceship_Asset.png', 64, 64);
    },

    create: function() {
        game.state.start('menu');
    }
}