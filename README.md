﻿# Raiden

## What I've Done
|Component|Score|Y/N|
|:-:|:-:|:-:|
|complete game process|15%|Y|
|basic rules|20%|Y|
|jucify mechanisms|15%|Y|
|animations/particle systems|each 10%|Y|
|UI|5%|Y|
|Others|-%|N|

## Jucify mechanisms

1. level : 每擊中敵人一次加十分，一百分即會進入第二階段，敵人將有特殊攻擊模式，一次發射多顆子彈，增加遊玩難度
2. skill : 第二階段時也會開啟玩家大絕-EMP電磁風暴，按下空白鍵(space)即可觸發，EMP電磁風暴將使場上所有敵人遭受超高能電磁波攻擊，能夠一次掃除範圍內所有敵人，注意：技能CD時間為7秒
3. 動畫和particle system ： player向前移動(up arrow key)時，會有噴射推進動畫，另外，當子彈打中敵人時或玩家被敵人打中時都會有particle effect
4. 請使用FireFox開啟確保顯示校果為最佳