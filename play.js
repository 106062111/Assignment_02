var playState = {
    create: function() {
        game.stage.backgroundColor = '#3498db';
        this.back = game.add.tileSprite(0, 0, 650, 650, 'background'); 
        //game.add.image(0, 0, 'background_cover');

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.Rkey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        this.Qkey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        this.spacekey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        this.player = game.add.sprite(game.width/2, game.height-100, 'player');
        this.player.facingLeft = false;
        this.player.speedup = false;
        this.player.hp = 3;
        this.player.anchor.setTo(0.5);
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;



        /// ToDo 3: Add 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        // this.player.animations.add('rightwalk', [1, 2], 8, true);
        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        // this.player.animations.add('leftwalk', [3, 4], 8, true);
        /// 3. Create the 'rightjump' animation with frame rate = 16 (frames 5 and 6 and no loop)
        this.player.animations.add('up', [9, 10], 12, false);
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        // this.player.animations.add('leftjump', [4, 6], 16, false);
        ///

        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(20, 'bullet');
        this.bulletPool.setAll('anchor.x', 0.5);
        this.bulletPool.setAll('anchor.y', 0.5);
        this.bulletPool.setAll('checkWorldBounds', true);
        this.bulletPool.setAll('outOfBoundsKill', true);
        game.time.events.loop(250, this.fire, this);

        this.enemy_bulletPool = game.add.group();
        this.enemy_bulletPool.enableBody = true;
        this.enemy_bulletPool.createMultiple(50, 'enemy_bullet');
        this.enemy_bulletPool.setAll('anchor.x', 0.5);
        this.enemy_bulletPool.setAll('anchor.y', 0.5);
        this.enemy_bulletPool.setAll('scale.x', 0.3);
        this.enemy_bulletPool.setAll('scale.y', 0.3);
        this.enemy_bulletPool.setAll('checkWorldBounds', true);
        this.enemy_bulletPool.setAll('outOfBoundsKill', true);

        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        this.enemies.forEach(e => {
            e.nextShotAt = 0;
            e.hp = 3;
        });
        this.shotDelay = 1500;
        game.time.events.loop(2000, this.addEnemy, this);

        this.show_result = false;
        this.score = 0;
        this.levelUP = false;
        this.skillCD = 420;
        this.skillcount = 0;
        this.text_s = game.add.text(10, 15, 'Score: 0', {font: '24px OCR A Extended', fill: '#ffffff'});
        this.text_h = game.add.text(10, 50, 'HP: 3', {font: '24px OCR A Extended', fill: '#ffffff'});
        this.text_l = game.add.text(10, 85, 'Level: 1', {font: '24px OCR A Extended', fill: '#ffffff'});
        this.text_skill = game.add.text(10, 120, 'Skill_EMP: Ready', {font: '24px OCR A Extended', fill: '#ffffff'});
        this.text_skill.visible = false;
    },

    hit_enemy: function(bullet, enemy) {
        bullet.kill();
        this.triggerParticle(bullet.x, bullet.y);
        enemy.hp -= 1;
        this.score += 10;
        this.text_s.text = 'Score: '+this.score;
        if(this.score >= 100) {
            this.levelUP = true;
            this.text_l.text = 'Level: 2';
            this.text_skill.visible = true;
            this.spacekey.onDown.add(this.triggerSkill ,this);
        }
        if(enemy.hp == 0) {
            enemy.hp = 3;
            enemy.kill();
        }
    },

    triggerSkill: function() {
        if(this.skillcount == 0){
            game.camera.shake(0.02, 1000);
            this.enemies.forEach(e => {
                if(e.alive) {
                    e.kill();
                    this.score += 30;
                    this.text_s.text = 'Score: '+this.score;
                }
            });
            this.enemy_bulletPool.forEach(e => {
                if(e.alive) {
                    e.kill();
                }
            });
            this.skillcount = this.skillCD;
            // console.log(this.skillcount/60);
            // this.text_skill.text = `Skill_EMP: ${this.skillcount/60}sec`;
        }
    },

    hit_player: function(player, bullet) {
        bullet.kill();
        this.triggerParticle(bullet.x, bullet.y);
        player.hp -= 1;
        this.text_h.text = "HP: "+player.hp;
        if(!this.player.hp) {
            player.kill();
            player.hp = 3;
            this.playerDie();
        }
    },

    destroy_player_enemy: function(player, enemy) {
        enemy.kill();
        player.kill();
        this.playerDie();
    },
    playerDie: function() {
        this.show_result = true;
        var over = game.add.text(game.width/2, 250, 'Game Over', {font: '50px OCR A Extended', fill: '#ffffff'});
        over.anchor.setTo(0.5);
        var text1 = game.add.text(game.width/2, 500, 'Press R to restart', {font: '24px OCR A Extended', fill: '#ffffff'});
        text1.anchor.setTo(0.5);
        var text2 = game.add.text(game.width/2, 550, 'Press Q to quit', {font: '24px OCR A Extended', fill: '#ffffff'});
        text2.anchor.setTo(0.5);
        this.Rkey.onDown.add(function(){game.state.start('play');}, this);
        this.Qkey.onDown.add(function(){game.state.start('menu');}, this);
    },

    fire: function() {
        if(!this.player.alive)
            return;
        var bullet = this.bulletPool.getFirstExists(false);
        if(!bullet) {
            bullet = game.add.sprite(-100, -100, 'bullet', this.bulletPool);
            game.physics.arcade.enable(bullet);
        }
        bullet.reset(this.player.x, this.player.y-20);
        bullet.body.velocity.y = -200;
    },

    addEnemy: function() {
        var enemy = this.enemies.getFirstDead();
        if(!enemy)
            return;
        enemy.anchor.setTo(0.5);
        enemy.scale.setTo(2, 2);
        enemy.reset(game.rnd.integerInRange(64, game.width-32), 0);
        enemy.body.velocity.y = 100;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },

    enemyfire: function() {
        this.enemies.forEach(e => {
            // console.log(e.nextShotAt);
            if(!e.alive || e.nextShotAt > game.time.now) return;
            e.nextShotAt = game.time.now + this.shotDelay;
            if(!this.levelUP){
                var bullet = this.enemy_bulletPool.getFirstExists(false);
                if(!bullet) {
                    bullet = game.add.sprite(-100, -100, 'enemy_bullet', this.enemy_bulletPool);
                    game.physics.arcade.enable(bullet);
                    bullet.checkWorldBounds = true;
                    bullet.outOfBoundsKill = true;
                }
                bullet.reset(e.x, e.y+30);
                bullet.body.velocity.y = 200;
            }
            else{
                var bullet1 = this.enemy_bulletPool.getFirstExists(false);
                if(!bullet1) {
                    bullet1 = game.add.sprite(-100, -100, 'enemy_bullet', this.enemy_bulletPool);
                    game.physics.arcade.enable(bullet);
                    bullet1.checkWorldBounds = true;
                    bullet1.outOfBoundsKill = true;
                }
                bullet1.reset(e.x, e.y+30);
                bullet1.body.velocity.y = 200;
                
                var bullet2 = this.enemy_bulletPool.getFirstExists(false);
                if(!bullet2) {
                    bullet2 = game.add.sprite(-100, -100, 'enemy_bullet', this.enemy_bulletPool);
                    game.physics.arcade.enable(bullet);
                    bullet2.checkWorldBounds = true;
                    bullet2.outOfBoundsKill = true;
                }
                bullet2.reset(e.x, e.y+30);
                bullet2.body.velocity.y = 200;
                bullet2.body.velocity.x = -50;

                var bullet3 = this.enemy_bulletPool.getFirstExists(false);
                if(!bullet3) {
                    bullet3 = game.add.sprite(-100, -100, 'enemy_bullet', this.enemy_bulletPool);
                    game.physics.arcade.enable(bullet);
                    bullet3.checkWorldBounds = true;
                    bullet3.outOfBoundsKill = true;
                }
                bullet3.reset(e.x, e.y+30);
                bullet3.body.velocity.y = 200;
                bullet3.body.velocity.x = 50;
            }
        });
    },

    triggerParticle: function(x, y) {

        /// Particle
        this.emitter = game.add.emitter(x, y, 20);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-100, 100);
        this.emitter.setXSpeed(-100, 100);
        this.emitter.setScale(1, 0, 1, 0, 500);
        // this.emitter.gravity = 500;
        this.emitter.start(true, 500, null, 20);
        ///
    },
    update: function() {
        if(this.levelUP && this.skillcount > 0) {
            this.skillcount--;
            this.text_skill.text = `Skill_EMP: ${(this.skillcount/60).toFixed(0)}sec`;
        }
        else if(this.levelUP && this.skillcount == 0) {
            this.text_skill.text = `Skill_EMP: Ready`;
        }
        
        this.back.tilePosition.y += 2;
        this.enemyfire();
        game.physics.arcade.overlap(this.bulletPool, this.enemies, this.hit_enemy, null, this);
        game.physics.arcade.overlap(this.player, this.enemy_bulletPool, this.hit_player, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.destroy_player_enemy, null, this);
        /// ToDo 6: Add collision 
        /// 1. Add collision between player and walls
        /// 2. Add collision between player and floor
        // game.physics.arcade.collide(this.player, this.floor);
        /// 3. Add collision between player and yellowBlock and add trigger animation "blockTween"
        // game.physics.arcade.collide(this.player, this.yellowBlock, this.blockTween, null, this);
        /// 4. Add collision between player and blueBlock and add trigger animation "blockParticle"
        // game.physics.arcade.collide(this.player, this.blueBlock, this.blockParticle, null, this);
        ///

        this.movePlayer();
    },

    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -250;
            this.player.body.velocity.y = 0;
            this.player.facingLeft = true;
            this.player.speedup = false;
            this.player.frame = 8;
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 250;
            this.player.body.velocity.y = 0;
            this.player.facingLeft = false;
            this.player.speedup = false;
            this.player.frame = 8;
        }

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            this.player.body.velocity.y = -200;
            this.player.body.velocity.x = 0;
            if(this.player.speedup == false)
                this.player.animations.play('up');
            this.player.speedup = true;
        }
        else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = 200;
            this.player.body.velocity.x = 0;
            this.player.speedup = false;
            this.player.frame = 8;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.player.speedup = false;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 8;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 8;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    }
};