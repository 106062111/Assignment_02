var menuState = {
    create: function() {
        this.cursor = game.input.keyboard.createCursorKeys();
        game.add.image(0, 0, 'menubg');
        var title = game.add.text(game.width/2, 120, 'Raiden', {font: '50px OCR A Extended', fill: '#ffffff'});
        title.anchor.setTo(0.5);
        var text = game.add.text(game.width/2, 500, 'Press UP to start', {font: '24px OCR A Extended', fill: '#ffffff'});
        text.anchor.setTo(0.5);
    },
    update: function() {
        if(this.cursor.up.isDown){
            game.state.start('play');
        }
    }
}