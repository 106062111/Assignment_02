var game = new Phaser.Game(650, 650, Phaser.AUTO, 'canvas');
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.start('load');